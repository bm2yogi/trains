TrainsApp

Design notes:
Graphs Routes and Links: I opted to keep the design simple and not use the traditional Node types. Instead, the Graph is composed of edges (links) keyed by their origins. Why? Because I wrote this solution applying a strict test-first approach and I was able to make the tests pass as-is. Refactoring working code to use a Node data type added no value, so I left it as is.

GraphQueries as extension methods: All code for the solution (including the algorithms) started in the test class. Separating data from behavior into pure, static methods help keep the Graph object "open for extension and closed for modification."

Main Program: Validation of user input and error handling was considered, but kept to a minimum, considering the limited scope of the application.

Running the App: Unzip, compile, and execute from the bin directory.