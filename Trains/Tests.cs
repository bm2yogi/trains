﻿using System.Linq;
using NUnit.Framework;
using Sahara;

namespace Trains
{
    [TestFixture]
    public class Tests
    {
        protected string[] Links;

        protected Graph Graph = new Graph();

        [TestFixtureSetUp]
        public void BeforeAll()
        {
            Links = new[] { "AB5", "BC4", "CD8", "DC8", "DE6", "AD5", "CE2", "EB3", "AE7" };
            Graph.AddLinks(Links);
        }

        [Test]
        public void BuildGraph()
        {
            var graph = new Graph();

            graph.AddLinks(Links);

            graph.LinksFor("A").Count().ShouldEqual(3);
            graph.LinksFor("B").Count().ShouldEqual(1);
            graph.LinksFor("C").Count().ShouldEqual(2);
            graph.LinksFor("D").Count().ShouldEqual(2);
            graph.LinksFor("E").Count().ShouldEqual(1);

            graph.Link("AB").Distance.ShouldEqual(5);
            graph.Link("BC").Distance.ShouldEqual(4);
            graph.Link("CD").Distance.ShouldEqual(8);
            graph.Link("DC").Distance.ShouldEqual(8);
            graph.Link("DE").Distance.ShouldEqual(6);
            graph.Link("AD").Distance.ShouldEqual(5);
            graph.Link("CE").Distance.ShouldEqual(2);
            graph.Link("EB").Distance.ShouldEqual(3);
            graph.Link("AE").Distance.ShouldEqual(7);
        }

        [Test]
        public void Distance_for_route_ABC()
        {
            Graph.Route("ABC").Distance.ShouldEqual(9);
        }

        [Test]
        public void Distance_for_route_ADC()
        {
            Graph.Route("ADC").Distance.ShouldEqual(13);
        }

        [Test]
        public void Distance_for_route_AEBCD()
        {
            Graph.Route("AEBCD").Distance.ShouldEqual(22);
        }

        [Test]
        public void Distance_for_route_AED()
        {
            Graph.Route("AED").ShouldBeNull();
        }

        [Test]
        public void Distance_for_route_AD()
        {
            Graph.Link("AD").Distance.ShouldEqual(5);
        }

        [Test]
        public void Number_of_trips_with_no_more_than_three_stops_for_CC()
        {
            Graph.TripsAndStops("CC", 3).Sum(s => s.Value).ShouldEqual(2);
        }

        [Test]
        public void Number_of_trips_with_four_stops_for_AC()
        {
            Graph.TripsAndStops("AC", 4)[4].ShouldEqual(3);
        }

        [Test]
        public void Number_of_unique_routes_shorter_than_30_for_CC()
        {
            Graph.NumberOfRoutesWithMaximumDistance("CC", 30).ShouldEqual(7);
        }

        [Test]
        public void Shortest_distance_for_AC()
        {
            Graph.ShortestRouteFor("AC").ShouldEqual(9);
        }

        [Test]
        public void Shortest_distance_for_BB()
        {
            Graph.ShortestRouteFor("BB").ShouldEqual(9);
        }
    }
}