﻿namespace Trains
{
    public class Link
    {
        public Link(string linkSpec)
        {
            Destination = linkSpec.Substring(1, 1);
            Distance = int.Parse(linkSpec.Substring(2, 1));
        }

        public string Destination { get; private set; }
        public int Distance { get; private set; }
    }
}