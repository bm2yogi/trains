﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Trains
{
    public class Route
    {
        private readonly List<Link> _links = new List<Link>();
        
        public Route(List<Link> links)
        {
            _links = links;
        }

        public string Origin { get; set; }

        public void AddLink(Link link)
        {
            _links.Add(link);
        }

        public Double Distance
        {
            get
            {
                return (!_links.Any())
                           ? Double.PositiveInfinity
                           : _links.Sum(l => l.Distance);
            }
        }

        public int Stops
        {
            get { return _links.Count() - 1; }
        }
    }
}