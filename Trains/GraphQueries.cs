using System;
using System.Collections.Generic;
using System.Linq;
using Trains;

public static class GraphQueries
{
    public static double ShortestRouteFor(this Graph graph, string routeSpec)
    {
        var origin = routeSpec.Substring(0, 1);
        var destination = routeSpec.Substring(1, 1);
        var visitedNodes = "";
        var distanceTo = graph.Nodes.ToDictionary(n => n, n => Double.PositiveInfinity);
        distanceTo[origin] = 0;

        var currentNode = origin;

        while (true)
        {
            foreach (var link in graph.LinksFor(currentNode))
            {
                distanceTo[link.Destination] = (Double.IsPositiveInfinity(distanceTo[link.Destination]))
                                                   ? distanceTo[currentNode] + link.Distance :
                                                   Math.Min(distanceTo[currentNode] + link.Distance, distanceTo[link.Destination]);
            }

            visitedNodes += currentNode;

            if (visitedNodes.Length == distanceTo.Count(n => !Double.IsPositiveInfinity(n.Value))) break;

            if (visitedNodes == origin && origin == destination)
            {
                visitedNodes = "";
                distanceTo[destination] = Double.PositiveInfinity;
            }

            currentNode = distanceTo
                .Where(n => !visitedNodes.Contains(n.Key))
                .Where(n => !Double.IsPositiveInfinity(n.Value))
                .OrderBy(n => n.Value).First().Key;
        }

        return distanceTo[destination];
    }

    public static Dictionary<int, int> TripsAndStops(this Graph graph, string routeSpec, int maxStopCount)
    {
        var origin = routeSpec.Substring(0, 1);
        var destination = routeSpec.Substring(1, 1);
        var stats = new Dictionary<int, int>();

        var currentSet = new [] {origin};

        for (var stopCount = 0; stopCount < maxStopCount; stopCount++)
        {
            currentSet = currentSet.SelectMany(graph.LinksFor)
                                   .Select(s => s.Destination)
                                   .ToArray();

            stats.Add(stopCount + 1, currentSet.Count(s=>s==destination));
        }

        return stats;
    }

    public static int NumberOfRoutesWithMaximumDistance(this Graph graph, string routeSpec, int maxDistance)
    {
        var routeCount = 0;
        var queue = new Queue<dynamic>();
        var origin = routeSpec.Substring(0, 1);
        var destination = routeSpec.Substring(1, 1);
        var current = new { Destination = origin, Distance = 0 };

        queue.Enqueue(current);

        while (queue.Count > 0)
        {
            current = queue.Dequeue();
            var next = graph.LinksFor(current.Destination).Select(l => new { l.Destination, l.Distance });

            foreach (var link in next)
            {
                if (current.Distance + link.Distance >= maxDistance) continue;

                if (current.Distance > 0 && link.Destination == destination) routeCount++;

                queue.Enqueue(new { link.Destination, Distance = current.Distance + link.Distance });
            }
        }
        return routeCount;
    }
}