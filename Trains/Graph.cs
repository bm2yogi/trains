﻿using System.Collections.Generic;
using System.Linq;

namespace Trains
{
    public class Graph
    {
        private readonly Dictionary<string, List<Link>> _links = new Dictionary<string, List<Link>>();

        public IEnumerable<string> Nodes
        {
            get { return _links.Keys; }
        }

        public IEnumerable<Link> LinksFor(string origin)
        {
            return (_links.ContainsKey(origin))
                       ? _links[origin]
                       : new List<Link>();
        }

        public void AddLinks(IEnumerable<string> links)
        {
            links.ToList().ForEach(AddLink);
        }

        private void AddLink(string linkSpec)
        {
            if (string.IsNullOrEmpty(linkSpec)) return;
            
            linkSpec = linkSpec.Trim();

            var origin = linkSpec.Substring(0, 1);

            if (!_links.ContainsKey(origin))
            {
                _links.Add(origin, new List<Link>());
            }

            _links[origin].Add(new Link(linkSpec));
        }

        public Route Route(string routeSpec)
        {
            var index = 0;
            var links = new List<Link>();

            while (index <= routeSpec.Length - 2)
            {
                links.Add(Link(routeSpec.Substring(index, 2)));
                index++;
            }

            return (links.Any(l => l == null))
                       ? null
                       : new Route(links);
        }

        public Link Link(string linkSpec)
        {
            var origin = linkSpec.Substring(0, 1);
            var destination = linkSpec.Substring(1, 1);

            return _links[origin].FirstOrDefault(l => l.Destination == destination);
        }
    }
}