﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Trains;

namespace TrainsApp
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0 || Regex.IsMatch(args[0], @"[-/][h|H|\?]"))
                ShowHelp();
            else
            {
                RunWithIt(args[0]);
            }
        }

        private static void RunWithIt(string filePath)
        {
            try
            {
                var inputs = ReadInputFile(filePath);
                var graph = LoadGraph(inputs);
                ShowAnswers(graph);
            }
            catch (Exception exception)
            {
                Console.WriteLine("Sorry, something went horribly wrong:{0}", exception.Message);
                Console.WriteLine();
                ShowHelp();
            }
        }

        private static IEnumerable<string> ReadInputFile(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
                throw new ArgumentException("No file was specified");

            if (!File.Exists(filePath))
                throw new ArgumentException("File does not exist", filePath);

            var file = File.OpenText(filePath).ReadLine();

            if (string.IsNullOrEmpty(file))
                throw new ApplicationException(String.Format("File {0} contains no data.", filePath));

            return file.Split(',').Select(s => s.Trim().ToUpperInvariant());
        }

        private static Graph LoadGraph(IEnumerable<string> inputs)
        {
            var graph = new Graph();
            graph.AddLinks(inputs);
            return graph;
        }

        private static void ShowAnswers(Graph graph)
        {
            Console.WriteLine("Output #1:{0}", ShowRouteDistance(graph, "ABC"));
            Console.WriteLine("Output #2:{0}", ShowRouteDistance(graph, "AD"));
            Console.WriteLine("Output #3:{0}", ShowRouteDistance(graph, "ADC"));
            Console.WriteLine("Output #4:{0}", ShowRouteDistance(graph, "AEBCD"));
            Console.WriteLine("Output #5:{0}", ShowRouteDistance(graph, "AED"));
            Console.WriteLine("Output #6:{0}", graph.TripsAndStops("CC", 3).Sum(s => s.Value));
            Console.WriteLine("Output #7:{0}", graph.TripsAndStops("AC", 4)[4]);
            Console.WriteLine("Output #8:{0}", graph.ShortestRouteFor("AC"));
            Console.WriteLine("Output #9:{0}", graph.ShortestRouteFor("BB"));
            Console.WriteLine("Output #10:{0}", graph.NumberOfRoutesWithMaximumDistance("CC", 30));
        }

        private static string ShowRouteDistance(Graph graph, string routeSpec)
        {
            var route = graph.Route(routeSpec);
            return (route == null)
                       ? "NO SUCH ROUTE"
                       : route.Distance.ToString(CultureInfo.InvariantCulture);
        }

        private static void ShowHelp()
        {
            Console.WriteLine(@"Trains App:
A directed graph where a node represents a town and an edge represents a route between two towns.

For the test input, the towns are named using the first few letters of the alphabet from A to D.  A route between two towns (A to B) with a distance of 5 is represented as AB5.

The test input should have one line, formatted as follows: 'AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7'

Usage: TrainApp.exe <pathtofile>");
        }
    }
}
